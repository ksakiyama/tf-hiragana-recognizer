import os
import sys
import random
import argparse
import cv2
import numpy as np
import tensorflow as tf


# Parameters
n_input = 32 * 32
n_classes = 71


def read_data(filename):
    images = []
    labels = []
    with open(filename, 'r') as f:
        for line in f.readlines():
            line = line.rstrip('\n')
            imgpath, label = line.split(' ')
            images.append(imgpath)
            labels.append(int(label))
    return images, labels


# Create some wrappers for simplicity
def conv2d(x, W, b, strides=1):
    # Conv2D wrapper, with bias and relu activation
    x = tf.nn.conv2d(x, W, strides=[1, strides, strides, 1], padding='VALID')
    return tf.nn.bias_add(x, b)


def maxpool2d(x, k=2):
    # MaxPool2D wrapper
    return tf.nn.max_pool(x, ksize=[1, k, k, 1], strides=[1, k, k, 1],
                          padding='SAME')


# Create model
def conv_net(x, weights, biases, dropout):
    # Reshape input picture
    x = tf.reshape(x, shape=[-1, 32, 32, 1])

    # Convolution Layer
    h = conv2d(x, weights['wc1'], biases['bc1'])
    h = tf.nn.relu(h)
    h = conv2d(h, weights['wc2'], biases['bc2'])
    h = tf.nn.relu(h)
    h = maxpool2d(h, k=2)
    h = tf.nn.dropout(h, dropout)

    # Convolution Layer
    h = conv2d(h, weights['wc3'], biases['bc3'])
    h = tf.nn.relu(h)
    h = conv2d(h, weights['wc4'], biases['bc4'])
    h = tf.nn.relu(h)
    h = maxpool2d(h, k=2)
    h = tf.nn.dropout(h, dropout)

    # Fully connected layer
    # Reshape conv2 output to fit fully connected layer input
    h = tf.reshape(h, [-1, weights['wd1'].get_shape().as_list()[0]])
    h = tf.add(tf.matmul(h, weights['wd1']), biases['bd1'])
    h = tf.nn.relu(h)
    # Apply Dropout
    h = tf.nn.dropout(h, dropout)

    # Output, class prediction
    h = tf.add(tf.matmul(h, weights['out']), biases['out'])
    return h


# Store layers weight & bias
# weights = {
#     'wc1': tf.Variable(tf.random_normal([3, 3, 1, 32])),
#     'wc2': tf.Variable(tf.random_normal([3, 3, 32, 32])),
#     'wc3': tf.Variable(tf.random_normal([3, 3, 32, 64])),
#     'wc4': tf.Variable(tf.random_normal([3, 3, 64, 64])),
#     # 64inputs, finally 5x5 image
#     'wd1': tf.Variable(tf.random_normal([5 * 5 * 64, 256])),
#     'out': tf.Variable(tf.random_normal([256, n_classes]))
# }
# weights = {
#     'wc1': tf.Variable(tf.truncated_normal([3, 3, 1, 32], stddev=0.1)),
#     'wc2': tf.Variable(tf.truncated_normal([3, 3, 32, 32], stddev=0.1)),
#     'wc3': tf.Variable(tf.truncated_normal([3, 3, 32, 64], stddev=0.1)),
#     'wc4': tf.Variable(tf.truncated_normal([3, 3, 64, 64], stddev=0.1)),
#     # 64inputs, finally 5x5 image
#     'wd1': tf.Variable(tf.truncated_normal([5 * 5 * 64, 256], stddev=0.1)),
#     'out': tf.Variable(tf.truncated_normal([256, n_classes], stddev=0.1))
# }
weights = {
    'wc1': tf.Variable(tf.truncated_normal([3, 3, 1, 32], stddev=0.05)),
    'wc2': tf.Variable(tf.truncated_normal([3, 3, 32, 32], stddev=0.05)),
    'wc3': tf.Variable(tf.truncated_normal([3, 3, 32, 64], stddev=0.05)),
    'wc4': tf.Variable(tf.truncated_normal([3, 3, 64, 64], stddev=0.05)),
    # 64inputs, finally 5x5 image
    'wd1': tf.Variable(tf.truncated_normal([5 * 5 * 64, 256], stddev=0.05)),
    'out': tf.Variable(tf.truncated_normal([256, n_classes], stddev=0.05))
}

# biases = {
#     'bc1': tf.Variable(tf.random_normal([32])),
#     'bc2': tf.Variable(tf.random_normal([32])),
#     'bc3': tf.Variable(tf.random_normal([64])),
#     'bc4': tf.Variable(tf.random_normal([64])),
#     'bd1': tf.Variable(tf.random_normal([256])),
#     'out': tf.Variable(tf.random_normal([n_classes]))
# }
# biases = {
#     'bc1': tf.Variable(tf.constant(0.1, shape=[32])),
#     'bc2': tf.Variable(tf.constant(0.1, shape=[32])),
#     'bc3': tf.Variable(tf.constant(0.1, shape=[64])),
#     'bc4': tf.Variable(tf.constant(0.1, shape=[64])),
#     'bd1': tf.Variable(tf.constant(0.1, shape=[256])),
#     'out': tf.Variable(tf.constant(0.1, shape=[n_classes]))
# }
biases = {
    'bc1': tf.Variable(tf.constant(0.0, shape=[32])),
    'bc2': tf.Variable(tf.constant(0.0, shape=[32])),
    'bc3': tf.Variable(tf.constant(0.0, shape=[64])),
    'bc4': tf.Variable(tf.constant(0.0, shape=[64])),
    'bd1': tf.Variable(tf.constant(0.0, shape=[256])),
    'out': tf.Variable(tf.constant(0.0, shape=[n_classes]))
}


class HiraganaDataset():

    def __init__(self, imagelist, labellist, prepros=False):
        self.imagelist = imagelist
        self.labellist = labellist
        self.n_classes = n_classes
        self.size = len(imagelist)
        self.i = 0
        self.prepros = prepros
        self.crop_size = 32
        self.canvas_size = 40
        self.start = int((self.canvas_size - self.crop_size) / 2)
        self.end = self.canvas_size - self.start

        self.shuffle = [i for i in range(0, self.size)]
        random.shuffle(self.shuffle)
        self.images = []
        self.labels = []

        # read all images
        for idx in range(self.size):
            self.images.append(self.__get_image_as_array(self.imagelist[idx]))
            self.labels.append(np.eye(self.n_classes)[self.labellist[idx]])

        self.images = np.asarray(self.images)
        self.labels = np.asarray(self.labels)

    def __get_image_as_array(self, imgpath):
        img = cv2.imread(imgpath, 0)
        return img.flatten().astype(np.float32) / 255

    def __preprocess_image(self, image):
        """
        画像の前処理を実施する
        """
        crop_size = self.crop_size
        canvas_size = self.canvas_size
        start = self.start
        end = self.end

        # -10〜10度で回転させ、そこからクロッピングする
        # 40x40のキャンバスの中央に文字貼り付け
        canvas = np.ones((canvas_size, canvas_size), dtype=np.float32)
        # TODO flatten()にしちゃってる
        canvas[start:end, start:end] = image.reshape(crop_size, crop_size)
        image = canvas

        # いったん白黒を反転（warpAffineの背景が黒のため仕方なく。。）
        image = 1 - image

        # 乱数で回転を実施
        rotate = random.randint(-10, 10)
        rotation_matrix = cv2.getRotationMatrix2D(
            (canvas_size / 2, canvas_size / 2), rotate, 1)
        image = cv2.warpAffine(
            image, rotation_matrix, (canvas_size, canvas_size))

        # 白黒を元に戻す
        image = 1 - image

        # 32x32を抜き出す
        dif = canvas_size - crop_size - 2
        top = random.randint(0, dif)
        left = random.randint(0, dif)
        bottom = top + crop_size
        right = left + crop_size
        image = image[top:bottom, left:right]
        # image = image.reshape(crop_size, crop_size, 1)

        return image.flatten()

    def next_batch(self, batch_size, all=True):
        images = []
        labels = []

        for idx in range(self.i, min(self.i + batch_size, self.size)):
            idx = self.shuffle[idx]
            labels.append(self.labels[idx])
            if self.prepros:
                images.append(self.__preprocess_image(self.images[idx]))
            else:
                images.append(self.images[idx])

        self.i += batch_size
        if self.i >= self.size:
            self.i = 0
            random.shuffle(self.shuffle)

        return np.asarray(images), np.asarray(labels)

    def test_batch(self):
        return self.images, self.labels


def main():
    parser = argparse.ArgumentParser(description='ひらがなの学習')
    parser.add_argument('train', help='Path to training image-label list file')
    parser.add_argument('val', help='Path to validation image-label list file')
    parser.add_argument('--batchsize', '-B', type=int, default=2048,
                        help='Learning minibatch size')
    parser.add_argument('--epoch', '-E', type=int, default=500,
                        help='Number of epochs to train')
    # parser.add_argument('--gpu', '-g', type=int, default=-1,
    #                     help='GPU ID (negative value indicates CPU')
    parser.add_argument('--out', '-o', default='result',
                        help='Output directory')
    args = parser.parse_args()

    if not os.path.exists(args.out):
        os.mkdir(args.out)

    batch_size = args.batchsize

    train_images, train_labels = read_data(args.train)
    test_images, test_labels = read_data(args.val)

    train_data = HiraganaDataset(train_images, train_labels, True)
    test_data = HiraganaDataset(test_images, test_labels)

    x = tf.placeholder(tf.float32, [None, n_input])
    y = tf.placeholder(tf.float32, [None, n_classes])
    keep_prob = tf.placeholder(tf.float32)  # dropout (keep probability)

    # Construct model
    pred = conv_net(x, weights, biases, keep_prob)

    # Define loss and optimizer
    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(pred, y))
    optimizer = tf.train.AdamOptimizer().minimize(cost)

    # Evaluate model
    correct_pred = tf.equal(tf.argmax(pred, 1), tf.argmax(y, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

    # Launch the graph
    with tf.Session() as sess:
        sess.run(tf.initialize_all_variables())

        n_steps = int(train_data.size / batch_size) + 1
        iter_count = 0

        for epoch in range(args.epoch):
            for step in range(n_steps):
                batch_x, batch_y = train_data.next_batch(batch_size)
                # run training
                sess.run(optimizer, feed_dict={x: batch_x, y: batch_y,
                                               keep_prob: 0.5})
                iter_count += 1

            # show epoch, iter
            print("epoch {:3d}, ".format(epoch) +
                  "iter {:6d}, ".format(iter_count), end='')

            # Calculate batch loss and accuracy
            batch_x, batch_y = train_data.test_batch()
            train_loss, train_acc = sess.run([cost, accuracy],
                                             feed_dict={x: batch_x,
                                             y: batch_y,
                                             keep_prob: 1.})

            print("Training Loss= {:.6f}, ".format(train_loss) +
                  "Training Accuracy= {:.5f}, ".format(train_acc),
                  end='')
            test_x, test_y = test_data.test_batch()
            test_acc = sess.run(accuracy, feed_dict={x: test_x,
                                                     y: test_y,
                                                     keep_prob: 1.})
            print("Testing Accuracy= {:.5f}".format(test_acc))

            tf.train.SummaryWriter(args.out, sess.graph)


if __name__ == '__main__':
    import sys
    sys.exit(int(main() or 0))
